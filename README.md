# python-funcional-class1-ex01

Exercícios do curso de Programação Funcional com o @dunossauro .


## Aula 1

### Exercícios 1

1. Fazer uma função que, composta por duas outras funções,
execute um cálculo de soma.

2. Montar uma função que aplica funções.

3. 



## Aula 2

### Exercícios 2

1. Fazer um jokenpo usando singledispatch

## Aula 3

### Exercícios 3

1. Criar um dispatch e usar register.

## Aula 4

### Exercícios 4

1. Montar um compose de funções
    * ler um arquivo de texto
    * fazer um parse do texto
    * contar palavras
    * contar letras

2. Fazer o exercício 3 da aula 1.
