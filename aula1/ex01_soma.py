'''
Fazer uma função que, composta por duas outras funções,
execute um cálculo de soma.
Author: regis
'''

def _soma(x, y):
    ''' Função de soma de dois valores. '''
    return x + y


def soma(x, z):
    '''
    Função que recebe a função _soma para somar dois valores.
    '''
    return _soma(x, z)


print(soma(1, 2))
print(soma(0, soma(0, 0)))
print(soma(1, soma(1, 1)))
print(soma(3, soma(3, 4)))
print(soma(10, soma(10, 10)))
print(soma(10, soma(10, soma(10, 10))))
print(soma(10, soma(10, soma(10, soma(10, 10)))))
print(soma(1, soma(2, soma(3, soma(4, soma(5, soma(6, soma(7, soma(8, soma(9, 10))))))))))
