'''
Montar um compose de funções
* ler um arquivo de texto
* fazer um parse do texto
* contar palavras
* contar letras
Author: regis
'''
from collections import Counter
from functools import partial
from re import sub


def compose(*funcs):
    def inner(arg):
        state = arg
        for func in reversed(funcs):
            state = func(state)
        return state
    return inner


def read_file(filename):
    ''' Lê o arquivo. '''
    with open(filename) as f:
        return f.read()


def remove_blank_lines(x):
    ''' Remove linhas em branco. '''
    return x.replace('\n', ' ')


def lower(x):
    ''' Transforma o texto em minúsculo. '''
    return x.lower()


def remove_punctuation(x):
    ''' Remove caracteres especiais. '''
    return sub(r'[^a-zA-Z0-9\s]', '', x)


def remove_spaces(x):
    ''' Remove espaços. '''
    return x.replace(' ', '')


def split_word(x):
    ''' Transforma texto numa lista. '''
    return x.split(' ')


def main():
    filename = 'arquivo.txt'
    texto = read_file(filename)
    filtro_de_pontuacao = compose(
        remove_blank_lines,
        lower,
        remove_punctuation,
    )
    count_l = compose(Counter, remove_spaces, filtro_de_pontuacao)
    count_w = compose(Counter, split_word, filtro_de_pontuacao)
    print(filtro_de_pontuacao(texto))
    print(count_l(texto))
    print(count_w(texto))


if __name__ == '__main__':
    main()
